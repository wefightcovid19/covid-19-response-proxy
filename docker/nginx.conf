user root;
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;

    keepalive_timeout  65;

    server {
        listen       80;
        server_name  "";
        resolver 127.0.0.11 ipv6=off;

        location /auth {
            proxy_pass http://auth:8080/auth;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;

            add_header 'Access-Control-Allow-Headers' "Origin, Content-Type, Accept, Authorization, Ionic-Platforms" always;
            if ($request_method = OPTIONS) {
                add_header Content-Length 0;
                add_header Content-Type text/plain;
                add_header Access-Control-Allow-Origin $http_origin;
                add_header Access-Control-Allow-Headers "Authorization, Content-Type, Ionic-Platforms";
                return 200;
            }
        }

        location /temperature-tracker {
            proxy_pass http://temperature-tracker:8080/temperature-tracker;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_http_version 1.1;

            add_header 'Access-Control-Allow-Origin' "$http_origin" always;
            add_header 'Access-Control-Allow-Headers' "Origin, Content-Type, Accept, Authorization, Ionic-Platforms, Timeout-Id" always;
            add_header 'Access-Control-Allow-Credentials' "true" always;
            add_header 'Access-Control-Allow-Methods' "GET, POST, PUT, DELETE, OPTIONS, HEAD" always;
            add_header 'Access-Control-Max-Age' "1209600" always;
            if ($request_method = OPTIONS) {
                add_header Content-Length 0;
                add_header Content-Type text/plain;
                add_header Access-Control-Allow-Methods "GET, POST, PUT, DELETE, OPTIONS";
                add_header Access-Control-Allow-Origin $http_origin;
                add_header Access-Control-Allow-Headers "Authorization, Content-Type, Ionic-Platforms, Timeout-Id";
                add_header Access-Control-Allow-Credentials true;
                return 200;
            }
        }
    }

    include servers/*;
}
